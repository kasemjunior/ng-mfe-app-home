import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BaseHrefFixPipe } from '../base-href-fix.pipe';
import { HomeComponent } from '../home/home.component';
import { MfeRoutingModule } from './mfe-routing.module';

@NgModule({
  declarations: [HomeComponent, BaseHrefFixPipe],
  imports: [CommonModule, MfeRoutingModule],
  providers: [{ provide: APP_BASE_HREF, useValue: environment.baseHref }]
})
export class MfeModule {}
