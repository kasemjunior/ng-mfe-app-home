import { APP_BASE_HREF } from '@angular/common';
import { Inject, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'baseHrefFix',
  pure: true
})
export class BaseHrefFixPipe implements PipeTransform {
  constructor(@Inject(APP_BASE_HREF) private baseHref: string) {}

  transform(value: string, ...args: any[]): string {
    return [this.baseHref, value].join('');
  }
}
